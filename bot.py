""" 
*help 
*reload
ython3 -m pip install discord.py
"""

import random
import discord
from discord.ext import commands
import json
import os
from model.func import *

bot = commands.Bot(
    command_prefix=setting_data['BOT_PREFIX'], case_insensitive=True)


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')


@bot.command()
async def load(ctx, extension):
    author_id = ctx.author.id
    if(id_check(author_id) == True):
        bot.load_extension(f'cmds.{extension}')
        await ctx.send(f'Loaded {extension}')


@bot.command()
async def unload(ctx, extension):
    author_id = ctx.author.id
    if(id_check(author_id) == True):
        bot.unload_extension(f'cmds.{extension}')
        await ctx.send(f'Unloaded {extension}')


@bot.command()
async def reload(ctx, extension):
    author_id = ctx.author.id
    if(id_check(author_id) == True):
        bot.reload_extension(f'cmds.{extension}')
        await ctx.send(f'Reloaded {extension}')

# help ending note
""" bot.help_command.get_ending_note """


for filename in os.listdir('./cmds'):
    if filename.endswith('.py'):
        bot.load_extension(f'cmds.{filename[:-3]}')

if __name__ == "__main__":
    bot.run(setting_data['TOKEN'])
