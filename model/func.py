import json
import ast

with open('./setting.json', 'r', encoding='utf8') as jfile:
    setting_data = json.load(jfile)
with open('./data/reply_cmds.json', 'r', encoding='utf8') as jfile:
    reply_data = json.load(jfile)


""" template = {'1王': {'資訊': {"header":"","footer":"","hp":600}, '報名列表': []}, 
'2王': {'資訊': {"header":"","footer":"","hp":800}, '報名列表': []}, 
'3王': {'資訊': {"header":"","footer":"","hp":1000}, '報名列表': []}, 
'4王': {'資訊': {"header":"","footer":"","hp":1200}, '報名列表': []}, 
'5王': {'資訊': {"header":"","footer":"","hp":1500}, '報名列表': []}} """

overflow = {'資訊': {"header": "", "footer": "", "hp": 90}, '報名列表': []}

OutKnife_Data = {}
OutKnife_Data[0] = {'1王': {'資訊': {"header": "", "footer": "", "hp": 600}, '報名列表': []},
                    '2王': {'資訊': {"header": "", "footer": "", "hp": 800}, '報名列表': []},
                    '3王': {'資訊': {"header": "", "footer": "", "hp": 1000}, '報名列表': []},
                    '4王': {'資訊': {"header": "", "footer": "", "hp": 1200}, '報名列表': []},
                    '5王': {'資訊': {"header": "", "footer": "", "hp": 1500}, '報名列表': []}}

OutKnife_Data[0]['補償清單'] = overflow

""" --------------- Initial Parameter --------------- """
All_OutKnife_Data = {}
now = {'周': 1, '王': 1}
list_msg_tmp = []  # [week, king, msg]
number_insert_msg = {}  # [msg.id] = [user_id, week, king, msg]
robot_id = 680100771085156366
""" --------------- Initial Parameter --------------- """

""" with open('./data.txt', 'r') as content_file:
    All_OutKnife_save_data = content_file.read()
All_OutKnife_Data = ast.literal_eval(All_OutKnife_save_data) """

for i in range(1, 100):
    All_OutKnife_Data[i] = {'1王': {'資訊': {"header": "", "footer": "", "hp": 600}, '報名列表': []},
                            '2王': {'資訊': {"header": "", "footer": "", "hp": 800}, '報名列表': []},
                            '3王': {'資訊': {"header": "", "footer": "", "hp": 1000}, '報名列表': []},
                            '4王': {'資訊': {"header": "", "footer": "", "hp": 1200}, '報名列表': []},
                            '5王': {'資訊': {"header": "", "footer": "", "hp": 1500}, '報名列表': []}}
    All_OutKnife_Data[i]['補償清單'] = overflow


def id_check(user_id):
    if user_id in setting_data["admin"]:
        return True
    return False
